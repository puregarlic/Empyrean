# Empyrean

This is the repository for the Empyrean game landing page. It **is not** the
game itself. If you're looking for the actual game, take a look over on GitHub
[here](https://github.com/gabrielkulp/OGPC2016).

For more information as to the game itself, check out the OGPC TMS page over
[here](http://tms.ogpc.info/Games/Details/db7a7690-1372-4447-987e-95736342dd10).

## Team

+	Gabriel Kulp
	+	Programmer
+	Adam Krivoshein
	+	3D Artist
+	Dylan Shumway
	+	Sound Design/Creation
+	Graham Barber
	+	Writer/Designer
